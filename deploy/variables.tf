variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops-nate"
}

variable "contact" {
  default = "boo0330@gmail.com"
}